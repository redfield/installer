// Copyright 2019 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"log/syslog"

	"gopkg.in/yaml.v2"

	"github.com/asticode/go-astilog"

	"gitlab.com/redfield/installer/pkg/gui"
	"gitlab.com/redfield/installer/pkg/installer"
)

var (
	installConfig = flag.String("install-config", "", "Install configuration file (yaml)")
)

func main() {
	flag.Parse()
	// Provide "-v" flag to get astilectron debug messages
	astilog.FlagInit()

	i := installer.InstallInfo{}

	installFile, err := ioutil.ReadFile(*installConfig)
	if err != nil {
		log.Fatalf("partitionFile.Get err   #%v ", err)
	}

	err = yaml.UnmarshalStrict(installFile, &i)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}

	logName := fmt.Sprintf("%s-installer", i.ProductName)
	logWriter, err := syslog.New(syslog.LOG_INFO, logName)

	if err == nil {
		log.SetOutput(logWriter)
	}

	if i.ValidDevices, err = installer.ListBlockDevices(i.IgnoredPrefixes); err != nil {
		log.Fatalf("Failed to retrieve valid block devices: %v", err)
	}

	if err = gui.Run(&i); err != nil {
		log.Fatalf("Installation error: %v", err)
	}
}
