// Copyright © 2019 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package auth

import (
	"github.com/pkg/errors"
)

// Option Simple key-value string pair
type Option struct {
	Key   string `yaml:"key"`
	Value string `yaml:"value"`
}

// LuksAuth Simple interface for obtaining a key to provide for cryptsetup
type Authenticator interface {
	SetOption(key string, value string) error
	GetOption(key string) string
	GetKey() ([]byte, error)
}

// NewAuthenticator Returns a new authenticator for use in encrypting/decrypting luks volumes
func NewAuthenticator(authType string, authOptions []Option) (Authenticator, error) {
	var auth Authenticator
	var err error

	switch authType {
	case "password":
		{
			pAuth, err := NewPasswordLuks()
			if err != nil {
				return auth, errors.WithMessage(err, "failed to construct luksauth interface")
			}

			auth = pAuth
		}
	case "usb":
		{
			uAuth, err := NewUSBLuks()
			if err != nil {
				return auth, errors.WithMessage(err, "failed to construct luksauth interface")
			}

			auth = uAuth
		}
	case "yubikey":
		{
			yAuth, err := NewYubikeyLuks()
			if err != nil {
				return auth, errors.WithMessage(err, "failed to construct luksauth interface")
			}

			auth = yAuth
		}
	default:
		{
			dAuth, err := NewDefaultLuks()
			if err != nil {
				return auth, errors.WithMessage(err, "failed to construct luksauth interface")
			}

			auth = dAuth
		}
	}

	for _, opt := range authOptions {
		err := auth.SetOption(opt.Key, opt.Value)
		if err != nil {
			return auth, err
		}
	}

	return auth, err
}
