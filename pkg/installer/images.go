// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package installer

import (
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"strings"

	"github.com/pkg/errors"
)

// InstallFile Describes a file for placement at a mounted destination
type InstallFile struct {
	SourceURI       string  `yaml:"source_uri"`
	DestinationFile string  `yaml:"destination_file"`
	Mounts          []Mount `yaml:"mount,flow"`
}

func (i InstallFile) String() string {
	return fmt.Sprintf("SourceURI: %v\nDestinationFile: %v\nMounts: %v", i.SourceURI, i.DestinationFile, i.Mounts)
}

// PlaceFile Mounts any necessary target volumes, obtains the source file
// using the provided credentials, places it at the required destination,
// and unmounts any mounted target volumes.
func PlaceFile(username string, password string, certificate string, i InstallFile, ignoreMountFailure bool) error {
	// Process required mounts for file placement
	for _, m := range i.Mounts {
		err := MountFileSystem(m)
		if err != nil && !ignoreMountFailure {
			return err
		}

		defer func(m Mount) {
			err := UnmountFileSystem(m)
			if err != nil {
				log.Printf("%v", err)
			}
		}(m)
	}

	uri, err := url.Parse(i.SourceURI)
	if err != nil {
		return nil
	}

	var response *http.Response

	switch uri.Scheme {
	case "file":
		{
			t := &http.Transport{}
			t.RegisterProtocol("file", http.NewFileTransport(http.Dir("/")))
			c := &http.Client{Transport: t}

			response, err = c.Get(uri.String())
			if err != nil {
				return err
			}

			defer response.Body.Close()
		}
	case "https", "http":
		{
			req, err := http.NewRequest(http.MethodGet, uri.String(), nil)
			if err != nil {
				return err
			}

			// With http, these credentials are sent in plaintext.
			if username != "" && password != "" {
				req.SetBasicAuth(username, password)
			}

			response, err = http.DefaultClient.Do(req)
			if err != nil {
				return err
			}

			defer response.Body.Close()

			if response.StatusCode != http.StatusOK {
				return errors.Errorf("failed to get %s: got %q", i.SourceURI, response.Status)
			}
		}
	default:
		return errors.Errorf("unsupported URI scheme: %s", uri.Scheme)
	}

	err = os.MkdirAll(path.Dir(i.DestinationFile), os.ModeDir)
	if err != nil {
		return err
	}

	f, err := os.Create(i.DestinationFile)
	if err != nil {
		return err
	}
	defer f.Close()

	if strings.HasSuffix(uri.EscapedPath(), ".gz") {
		zr, zErr := gzip.NewReader(response.Body)
		if zErr != nil {
			return zErr
		}

		_, err = io.Copy(f, zr)
	} else {
		_, err = io.Copy(f, response.Body)
	}

	return err
}

// PlaceFiles Place a set of files
func PlaceFiles(i InstallInfo) error {
	for _, f := range i.InstallFiles {
		if strings.HasSuffix(f.SourceURI, "/") && strings.HasSuffix(f.DestinationFile, "/") {
			src, err := url.Parse(f.SourceURI)
			if err != nil {
				return errors.Wrapf(err, "failed to parse URI: %v", src)
			}

			files, err := ioutil.ReadDir(src.RequestURI())
			if err != nil {
				return errors.Wrapf(err, "failed to read directory: %v", src.RequestURI())
			}

			for _, file := range files {
				newFile := InstallFile{
					SourceURI:       f.SourceURI + file.Name(),
					DestinationFile: f.DestinationFile + file.Name(),
					Mounts:          make([]Mount, len(f.Mounts))}

				copy(newFile.Mounts, f.Mounts)

				err := PlaceFile(i.Username, i.Password, i.Certificate, newFile, i.Upgrade)
				if err != nil {
					return errors.Wrapf(err, "failed to place file: %v %v", newFile.SourceURI, newFile.DestinationFile)
				}
			}

		} else {
			err := PlaceFile(i.Username, i.Password, i.Certificate, f, i.Upgrade)
			if err != nil {
				return errors.Wrapf(err, "failed to place file: %v %v", f.SourceURI, f.DestinationFile)
			}
		}
	}

	return nil
}
