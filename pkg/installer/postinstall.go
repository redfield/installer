// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package installer

import (
	"log"
	"os/exec"
	"strings"

	"github.com/pkg/errors"
)

type User struct {
	User     string `yaml:"user"`
	Password string `yaml:"password"`
}

func createUser(u User) error {
	m := Mount{
		Source: "/dev/mapper/root",
		Point:  "/tmp/dom0"}

	err := MountFileSystem(m)
	if err != nil {
		log.Printf("%v", err)
	}

	defer func(m Mount) {
		err := UnmountFileSystem(m)
		if err != nil {
			log.Printf("%v", err)
		}
	}(m)

	cmd := exec.Command("useradd", "--root", "/tmp/dom0", "--shell", "/bin/login", strings.Split(u.User, "@")[0])

	stdErrStdOut, err := cmd.CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to run command: %s", stdErrStdOut)
	}

	return nil
}

func setPassword(u User) error {
	m := Mount{
		Source: "/dev/mapper/root",
		Point:  "/tmp/dom0"}

	err := MountFileSystem(m)
	if err != nil {
		log.Printf("%v", err)
	}

	defer func(m Mount) {
		err := UnmountFileSystem(m)
		if err != nil {
			log.Printf("%v", err)
		}
	}(m)

	cmd := exec.Command("chpasswd", "--root", "/tmp/dom0")

	stdin, err := cmd.StdinPipe()
	if err != nil {
		return errors.New("failed to get stdin")
	}

	go func() {
		defer stdin.Close()

		_, _ = stdin.Write([]byte(strings.Split(u.User, "@")[0] + ":" + u.Password))
	}()

	stdErrStdOut, err := cmd.CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to run command: %s", stdErrStdOut)
	}

	return nil
}

func CreateUsers(i InstallInfo) error {
	user := User{
		User:     i.Username,
		Password: i.Password}

	err := createUser(user)
	if err != nil {
		return errors.Wrapf(err, "failed to create user")
	}

	root := User{
		User:     "root",
		Password: i.RootPassword}

	err = setPassword(root)
	if err != nil {
		return errors.Wrapf(err, "failed to set root password")
	}

	err = setPassword(user)
	if err != nil {
		return errors.Wrapf(err, "failed to set user password")
	}

	return err
}
