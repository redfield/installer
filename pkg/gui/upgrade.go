// Copyright © 2019 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gui

import (
	"log"

	"gitlab.com/redfield/installer/pkg/installer"
)

// PerformInstallWithGUI Creates an installation according to the information in InstallInfo, spawns GUI window
func PerformUpgradeWithGUI(i installer.InstallInfo, ui *UI) error {
	ui.UpdateProgress(10, "Scanning logical volumes  ...")
	err := installer.InitializeLVM()
	if err != nil {
		return err
	}

	ui.UpdateProgress(20, "Decrypting volumes  ...")
	err = installer.OpenDiskCrypts(i.DiskCrypts, false)
	if err != nil {
		return err
	}
	defer func() {
		err := installer.CloseDiskCrypts(i.DiskCrypts, false)
		if err != nil {
			log.Printf("%v", err)
		}
	}()

	ui.UpdateProgress(30, "Creating filesystems  ...")
	err = installer.CreateFileSystems(i.FileSystems, "upgrade")
	if err != nil {
		return err
	}

	ui.UpdateProgress(40, "Configuring EFI  ...")
	err = installer.ConfigureEFI(i)
	if err != nil {
		return err
	}

	// Substitute the upgrade logical volume as
	// as the install target, rather than root
	installer.ChangeFileDestinations(&i)

	ui.UpdateProgress(50, "Installing files  ...")
	err = installer.PlaceFiles(i)
	if err != nil {
		return err
	}

	ui.UpdateProgress(90, "Moving logical volumes  ...")
	err = installer.RenameLogicalVolume(i.PhysicalVolumes, "dom0", "root", "oldroot")
	if err != nil {
		return err
	}

	err = installer.RenameLogicalVolume(i.PhysicalVolumes, "dom0", "upgrade", "root")
	if err != nil {
		return err
	}

	err = installer.RenameLogicalVolume(i.PhysicalVolumes, "dom0", "oldroot", "upgrade")
	if err != nil {
		return err
	}

	err = installer.CreateUsers(i)
	if err != nil {
		return err
	}

	ui.UpdateProgress(100, "Installation Complete")
	return nil
}
